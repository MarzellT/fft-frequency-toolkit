cmake_minimum_required(VERSION 3.24)
project(
        FFT
        VERSION 0.1.0
        DESCRIPTION "FFT Frequency Toolkit"
        LANGUAGES CXX
)

# Add helper files to cmake path
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

# Set up dependencies
include(SetUpDependencies)

# Require out-of-source builds
#########################################################
file(TO_CMAKE_PATH "${PROJECT_BINARY_DIR}/CMakeLists.txt" LOC_PATH)
if (EXISTS "${LOC_PATH}")
    message(FATAL_ERROR "You cannot build in a source directory (or any directory with a CMakeLists.txt file). Please make a build subdirectory. Feel free to remove CMakeCache.txt and CMakeFiles.")
endif ()


# Tests
add_subdirectory(src)
add_subdirectory(tests)

set(CMAKE_CXX_STANDARD 20)
