# FFT: FFT Frequency Toolkit

This library provides an implementation for the
[Discrete Fourier Transformation](https://en.wikipedia.org/wiki/Discrete_Fourier_transform).
   
At the moment there isn't really anything here but at some point there
will be at least one implementation.