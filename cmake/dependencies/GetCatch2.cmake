set(DOWNLOAD_CATCH2 OFF)

if (CATCH2_SOURCE STREQUAL "System")
    find_package(catch2 REQUIRED)
elseif (CATCH2_SOURCE STREQUAL "Auto")
    find_package(catch2)
else ()
    set(DOWNLOAD_CATCH2 ON)
endif ()

if (NOT DOWNLOAD_CATCH2)
    if (catch2_FOUND)
        if (CATCH2_VERSION)
            if (NOT CATCH2_VERSION VERSION_LESS_EQUAL v3.0.1 OR NOT CATCH2_VERSION VERSION_GREATER_EQUAL v3.0.1)
                if (CATCH2_SOURCE STREQUAL "System")
                    message(FATAL_ERROR "catch2 was found but has wrong version. Version >=v3.0.1 and <=v3.0.1 is required.")
                else ()
                    message(NOTICE "catch2 was found but has wrong version. Version >=v3.0.1 and <=v3.0.1 is required.
                Attempting download of catch2.")
                    set(DOWNLOAD_CATCH2 ON)
                endif ()
            endif ()
        else ()
            if (CATCH2_SOURCE STREQUAL "System")
                message(FATAL_ERROR "catch2 was found but version could not be determined.")
            else ()
                message(WARNING "catch2 was found but version could not be determined.
                Attempting download of catch2.")
                set(DOWNLOAD_CATCH2 ON)
            endif ()
        endif ()
    else ()
        message(STATUS "catch2 was not found. Attempting download of catch2.")
        set(DOWNLOAD_CATCH2 ON)
    endif ()
endif ()

if (DOWNLOAD_CATCH2)
    include(FetchContent)

    set(DEFAULT_CATCH2_VERSION v3.0.1)
    if (NOT USE_CATCH2_VERSION)
        set(USE_CATCH2_VERSION "${DEFAULT_CATCH2_VERSION}" CACHE STRING "Choose version for catch2." FORCE)
        set_property(CACHE USE_CATCH2_VERSION PROPERTY STRINGS
                "v3.0.1"
                )
    endif ()

    FetchContent_Declare(
            Catch2
            GIT_REPOSITORY https://github.com/catchorg/Catch2.git
            GIT_TAG ${USE_CATCH2_VERSION}
            SOURCE_DIR ${PROJECT_SOURCE_DIR}/extern/catch2
    )

    FetchContent_GetProperties(Catch2)

    string(TOLOWER "Catch2" LOWER_CASE_NAME)
    if (NOT ${LOWER_CASE_NAME}_FOUND)
        message(STATUS "Downloading catch2 ${USE_CATCH2_VERSION}")
        FetchContent_MakeAvailable(Catch2)
    endif ()
endif ()
