set(Catch2_SOURCE "Download" CACHE STRING "Specifies if Catch2 should be searched, downloaded, or both.")
set_property(CACHE Catch2_SOURCE PROPERTY STRINGS
        "Auto" "Download" "System")

include(dependencies/GetCatch2)