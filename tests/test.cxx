#include <catch2/catch_test_macros.hpp>
#include <fft.hxx>

TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( test( 1, 2) == 3 );
    REQUIRE( test( 2, 4) == 6 );
    REQUIRE( test( 3, 6) == 9 );
    REQUIRE( test(4, 8) == 12 );
}